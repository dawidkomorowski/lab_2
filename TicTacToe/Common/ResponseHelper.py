from Common.ResponseTypes import ResponseTypes
from Dto.GameMoveDto import GameMoveDto
from Dto.GameMoveDtoConverter import GameMoveDtoConverter
from Dto.GamePlayerDtoConverter import GamePlayerDtoConverter
from Network.Response import Response, ResponseStatus
from Xml.XmlSerializer import XmlSerializer


class ResponseHelper:
    @staticmethod
    def createResponse(type, data=None):
        if type == ResponseTypes.PlainOkResponse:
            return Response.create(status=ResponseStatus.Ok)

        if type == ResponseTypes.GameMoveResponse:
            if data is None:
                raise Exception("Missing argument.")

            gamePlayerDtoConverter = GamePlayerDtoConverter()
            gameMoveDtoConverter = GameMoveDtoConverter(gamePlayerDtoConverter)
            gameMoveDto = gameMoveDtoConverter.toDto(data)
            serializedGameMoveDto = XmlSerializer.serialize(gameMoveDto, GameMoveDto)
            return Response.create(data=serializedGameMoveDto, status=ResponseStatus.Ok)

        raise Exception("Unknown type: " + str(type))

    @staticmethod
    def extractData(response, type):
        if type == ResponseTypes.PlainOkResponse:
            return None

        if type == ResponseTypes.GameMoveResponse:
            gamePlayerDtoConverter = GamePlayerDtoConverter()
            gameMoveDtoConverter = GameMoveDtoConverter(gamePlayerDtoConverter)
            gameMoveDto = XmlSerializer.deserialize(response.data, GameMoveDto)
            return gameMoveDtoConverter.fromDto(gameMoveDto)

        raise Exception("Unknown type: " + str(type))
