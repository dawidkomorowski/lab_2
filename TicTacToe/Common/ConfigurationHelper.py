from Network.Core.Configuration import Configuration
from Xml.XmlSerializer import XmlSerializer


class ConfigurationHelper:
    @staticmethod
    def loadConfigurationFromFile(filepath):
        file = open(filepath, "r")
        xmlContent = file.read()
        configuration = XmlSerializer.deserialize(xmlContent, Configuration)
        file.close()
        return configuration
