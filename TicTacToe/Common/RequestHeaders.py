class RequestHeaders:
    DisplayPlainText = "tictactoe/plaintext"
    DisplayGameState = "tictactoe/gamestate"
    AskForGameMove = "tictactoe/gamemove"
    EndGameSession = "tictactoe/eot"
