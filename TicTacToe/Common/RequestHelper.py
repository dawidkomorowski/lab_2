from Common.RequestHeaders import RequestHeaders
from Dto.GameFieldDtoConverter import GameFieldDtoConverter
from Dto.GamePlayerDtoConverter import GamePlayerDtoConverter
from Dto.GameStateDto import GameStateDto
from Dto.GameStateDtoConverter import GameStateDtoConverter
from Network.Request import Request
from Xml.XmlSerializer import XmlSerializer


class RequestHelper:
    @staticmethod
    def createRequest(header, data=None):
        if header == RequestHeaders.DisplayPlainText:
            if data is None:
                raise Exception("Missing argument.")

            return Request.create(data, RequestHeaders.DisplayPlainText)

        if header == RequestHeaders.DisplayGameState:
            if data is None:
                raise Exception("Missing argument.")

            gamePlayerDtoConverter = GamePlayerDtoConverter()
            gameFieldDtoConverter = GameFieldDtoConverter()
            gameStateDtoConverter = GameStateDtoConverter(gamePlayerDtoConverter, gameFieldDtoConverter)
            gameStateDto = gameStateDtoConverter.toDto(data)
            serializedGameStateDto = XmlSerializer.serialize(gameStateDto, GameStateDto)
            return Request.create(serializedGameStateDto, RequestHeaders.DisplayGameState)
        if header == RequestHeaders.AskForGameMove:
            return Request.create(header=RequestHeaders.AskForGameMove)
        if header == RequestHeaders.EndGameSession:
            return Request.create(header=RequestHeaders.EndGameSession)

        raise Exception("Unknown header: " + header)

    @staticmethod
    def extractData(request):
        if request.header == RequestHeaders.DisplayPlainText:
            return request.data

        if request.header == RequestHeaders.DisplayGameState:
            gamePlayerDtoConverter = GamePlayerDtoConverter()
            gameFieldDtoConverter = GameFieldDtoConverter()
            gameStateDtoConverter = GameStateDtoConverter(gamePlayerDtoConverter, gameFieldDtoConverter)
            gameStateDto = XmlSerializer.deserialize(request.data, GameStateDto)
            return gameStateDtoConverter.fromDto(gameStateDto)

        if request.header == RequestHeaders.AskForGameMove:
            return None

        if request.header == RequestHeaders.EndGameSession:
            return None

        raise Exception("Unknown header: " + request.header)
