from unittest import TestCase

from Xml.UnitTests.TestData import *
from Xml.XmlSerializer import XmlSerializer


class TestXmlSerializer(TestCase):
    def test_deserialize_ShouldReturnNone_GivenIncorrectTypeForXml(self):
        # arrange
        xmlContent = """
<root/>
"""

        # act
        rootObject = XmlSerializer.deserialize(xmlContent, Root)

        # assert
        self.assertEqual(rootObject, None)

    def test_deserialize_ShouldReturnObjectOfCorrectClass_GivenXmlWithSimpleRootElementOnly(self):
        # arrange
        xmlContent = """
<Root/>
"""

        # act
        rootObject = XmlSerializer.deserialize(xmlContent, Root)

        # assert
        self.assertIsInstance(rootObject, Root)

    def test_deserialize_ShouldDeserializeObjectWithSingleAttribute(self):
        # arrange
        xmlContent = """
<RootWithSingleAttribute id='1'/>
"""

        # act
        rootObject = XmlSerializer.deserialize(xmlContent, RootWithSingleAttribute)

        # assert
        self.assertIsInstance(rootObject, RootWithSingleAttribute)
        self.assertEqual(rootObject.id, 1)

    def test_deserialize_ShouldDeserializeObjectWithManyAttributesOfDifferentTypes(self):
        # arrange
        xmlContent = """
<RootWithManyAttributes id='57' name='root' fitness='14.72' color='Red'/>
"""

        # act
        rootObject = XmlSerializer.deserialize(xmlContent, RootWithManyAttributes)

        # assert
        self.assertIsInstance(rootObject, RootWithManyAttributes)
        self.assertEqual(rootObject.id, 57)
        self.assertEqual(rootObject.name, "root")
        self.assertEqual(rootObject.fitness, 14.72)
        self.assertEqual(rootObject.color.string, "Red")

    def test_deserializer_ShouldDeserializeObjectWithSingleValue(self):
        # arrange
        xmlContent = """
<RootWithSingleValue>
    <id>4</id>
</RootWithSingleValue>
"""

        # act
        rootObject = XmlSerializer.deserialize(xmlContent, RootWithSingleValue)

        # assert
        self.assertIsInstance(rootObject, RootWithSingleValue)
        self.assertEqual(rootObject.id, 4)

    def test_deserialize_ShouldDeserializeObjectWithManyValuesOfDifferentTypes(self):
        # arrange
        xmlContent = """
<RootWithManyValues>
    <id>7</id>
    <name>XmlElement</name>
    <fitness>3.14</fitness>
    <color>Green</color>
</RootWithManyValues>
"""

        # act
        rootObject = XmlSerializer.deserialize(xmlContent, RootWithManyValues)

        # assert
        self.assertIsInstance(rootObject, RootWithManyValues)
        self.assertEqual(rootObject.id, 7)
        self.assertEqual(rootObject.name, "XmlElement")
        self.assertEqual(rootObject.fitness, 3.14)
        self.assertEqual(rootObject.color.string, "Green")

    def test_deserialize_ShouldDeserializeObjectWithSingleChild(self):
        # arrange
        xmlContent = """
<RootWithChild>
    <Child/>
</RootWithChild>
"""

        # act
        rootObject = XmlSerializer.deserialize(xmlContent, RootWithChild)

        # assert
        self.assertIsInstance(rootObject, RootWithChild)
        self.assertIsInstance(rootObject.child[0], Child)
        self.assertEqual(len(rootObject.child), 1)

    def test_deserialize_ShouldDeserializeObjectWithManyDifferentChildren(self):
        # arrange
        xmlContent = """
<RootWithManyChildren>
    <Child1/>
    <Child2/>
    <Child3/>
</RootWithManyChildren>
"""

        # act
        rootObject = XmlSerializer.deserialize(xmlContent, RootWithManyChildren)

        # assert
        self.assertIsInstance(rootObject, RootWithManyChildren)
        self.assertIsInstance(rootObject.child1[0], Child1)
        self.assertIsInstance(rootObject.child2[0], Child2)
        self.assertIsInstance(rootObject.child3[0], Child3)

    def test_deserialize_ShouldDeserializeObjectWithCollectionOfChildObjects(self):
        # arrange
        xmlContent = """
<RootWithChild>
    <Child/>
    <Child/>
    <Child/>
</RootWithChild>
"""

        # act
        rootObject = XmlSerializer.deserialize(xmlContent, RootWithChild)

        # assert
        self.assertIsInstance(rootObject, RootWithChild)
        self.assertIsInstance(rootObject.child[0], Child)
        self.assertIsInstance(rootObject.child[1], Child)
        self.assertIsInstance(rootObject.child[2], Child)
        self.assertEqual(len(rootObject.child), 3)

    def test_deserialize_ShouldDeserializeObjectWithNestedChildren(self):
        # arrange
        xmlContent = """
<RootWithNestedChildren>
    <FirstLevelChild>
        <SecondLevelChild>
            <ThirdLevelChild/>
        </SecondLevelChild>
    </FirstLevelChild>
</RootWithNestedChildren>
"""

        # act
        rootObject = XmlSerializer.deserialize(xmlContent, RootWithNestedChildren)

        # assert
        self.assertIsInstance(rootObject, RootWithNestedChildren)
        self.assertIsInstance(rootObject.child[0], FirstLevelChild)
        self.assertEqual(len(rootObject.child), 1)
        self.assertIsInstance(rootObject.child[0].child[0], SecondLevelChild)
        self.assertEqual(len(rootObject.child[0].child), 1)
        self.assertIsInstance(rootObject.child[0].child[0].child[0], ThirdLevelChild)
        self.assertEqual(len(rootObject.child[0].child[0].child), 1)

    def test_deserialize_ShouldDeserializeComplexObject(self):
        # arrange
        xmlContent = """
<Module name='Arithmetic'>
    <description>Set of classes targeting common arithmetical problems.</description>
    <Class name='Number' accessModifier='public'>
        <implementedInterfaces>Comparable, Serializable, Cloneable</implementedInterfaces>
        <Method name='Add' accessModifier='public'>
            <argsNumber>2</argsNumber>
            <argsNames>number1, number2</argsNames>
        </Method>
        <Method name='Subtract' accessModifier='public'>
            <argsNumber>2</argsNumber>
            <argsNames>number1, number2</argsNames>
        </Method>
    </Class>
    <Class name='Complex' accessModifier='public'>
        <implementedInterfaces>Comparable, Serializable, Cloneable</implementedInterfaces>
        <Method name='AsExponential' accessModifier='public'>
            <argsNumber>0</argsNumber>
        </Method>
    </Class>
    <Class name='NaN' accessModifier='internal'>
        <implementedInterfaces>Comparable</implementedInterfaces>
    </Class>
</Module>
"""

        # act
        rootObject = XmlSerializer.deserialize(xmlContent, Module)

        # assert
        self.assertIsInstance(rootObject, Module)
        self.assertEqual(rootObject.name, "Arithmetic")
        self.assertEqual(rootObject.description, "Set of classes targeting common arithmetical problems.")
        self.assertEqual(len(rootObject.classes), 3)

        self.assertIsInstance(rootObject.classes[0], Class)
        self.assertEqual(rootObject.classes[0].name, "Number")
        self.assertEqual(rootObject.classes[0].accessModifier, "public")
        self.assertEqual(rootObject.classes[0].implementedInterfaces, "Comparable, Serializable, Cloneable")
        self.assertEqual(len(rootObject.classes[0].methods), 2)

        self.assertIsInstance(rootObject.classes[0].methods[0], Method)
        self.assertEqual(rootObject.classes[0].methods[0].name, "Add")
        self.assertEqual(rootObject.classes[0].methods[0].accessModifier, "public")
        self.assertEqual(rootObject.classes[0].methods[0].argsNumber, 2)
        self.assertEqual(rootObject.classes[0].methods[0].argsNames, "number1, number2")

        self.assertIsInstance(rootObject.classes[0].methods[1], Method)
        self.assertEqual(rootObject.classes[0].methods[1].name, "Subtract")
        self.assertEqual(rootObject.classes[0].methods[1].accessModifier, "public")
        self.assertEqual(rootObject.classes[0].methods[1].argsNumber, 2)
        self.assertEqual(rootObject.classes[0].methods[1].argsNames, "number1, number2")

        self.assertIsInstance(rootObject.classes[1], Class)
        self.assertEqual(rootObject.classes[1].name, "Complex")
        self.assertEqual(rootObject.classes[1].accessModifier, "public")
        self.assertEqual(rootObject.classes[1].implementedInterfaces, "Comparable, Serializable, Cloneable")
        self.assertEqual(len(rootObject.classes[1].methods), 1)

        self.assertIsInstance(rootObject.classes[1].methods[0], Method)
        self.assertEqual(rootObject.classes[1].methods[0].name, "AsExponential")
        self.assertEqual(rootObject.classes[1].methods[0].accessModifier, "public")
        self.assertEqual(rootObject.classes[1].methods[0].argsNumber, 0)

        self.assertIsInstance(rootObject.classes[2], Class)
        self.assertEqual(rootObject.classes[2].name, "NaN")
        self.assertEqual(rootObject.classes[2].accessModifier, "internal")
        self.assertEqual(rootObject.classes[2].implementedInterfaces, "Comparable")
        self.assertEqual(len(rootObject.classes[2].methods), 0)

    def test_deserialize_ShouldDeserializeObjectWithChildAsSingleCollection(self):
        # arrange
        xmlContent = """<?xml version="1.0" ?>
<RootWithSingleChild>
\t<Child/>
</RootWithSingleChild>
"""

        # act
        rootObject = XmlSerializer.deserialize(xmlContent, RootWithSingleChild)

        # assert
        self.assertIsInstance(rootObject, RootWithSingleChild)
        self.assertIsInstance(rootObject.child, Child)

    def test_deserialize_ShouldDeserializeObjectWithComplexChildrenAsSingleCollection(self):
        # arrange
        xmlContent = """<?xml version="1.0" ?>
<RootWithSingleCollectionChildren>
\t<ChildChild>
\t\t<Child/>
\t</ChildChild>
\t<MethodChild>
\t\t<Method accessModifier="public" name="Add">
\t\t\t<argsNames>number1, number2</argsNames>
\t\t\t<argsNumber>2</argsNumber>
\t\t</Method>
\t</MethodChild>
</RootWithSingleCollectionChildren>
"""

        # act
        rootObject = XmlSerializer.deserialize(xmlContent, RootWithSingleCollectionChildren)

        # assert
        self.assertIsInstance(rootObject, RootWithSingleCollectionChildren)
        self.assertIsInstance(rootObject.childChild, ChildChild)
        self.assertIsInstance(rootObject.childChild.child, Child)
        self.assertIsInstance(rootObject.methodChild, MethodChild)
        self.assertIsInstance(rootObject.methodChild.method, Method)
        self.assertEqual(rootObject.methodChild.method.name, "Add")
        self.assertEqual(rootObject.methodChild.method.accessModifier, "public")
        self.assertEqual(rootObject.methodChild.method.argsNames, "number1, number2")
        self.assertEqual(rootObject.methodChild.method.argsNumber, 2)

    def test_deserialize_ShouldDeserializeObjectWithConstructor(self):
        # arrange
        xmlContent = """<?xml version="1.0" ?>
<RootWithConstructor>
\t<string>different string</string>
\t<integer>15</integer>
\t<Child/>
\t<Child1/>
\t<Child1/>
\t<Child1/>
</RootWithConstructor>
"""

        # act
        rootObject = XmlSerializer.deserialize(xmlContent, RootWithConstructor)

        # assert
        self.assertIsInstance(rootObject, RootWithConstructor)
        self.assertEqual(rootObject.string, "different string")
        self.assertEqual(rootObject.integer, 15)
        self.assertIsInstance(rootObject.object, Child)
        self.assertEqual(len(rootObject.objects), 3)
        for o in rootObject.objects:
            self.assertIsInstance(o, Child1)

    def test_serialize_ShouldReturnNone_GivenIncorrectTypeForObject(self):
        # arrange
        rootObject = Root()

        # act
        xmlContent = XmlSerializer.serialize(rootObject, Child)

        # assert
        self.assertEqual(xmlContent, None)

    def test_serialize_ShouldSerializeRootOnlyObject(self):
        # arrange
        rootObject = Root()
        expectedContent = """<?xml version="1.0" ?>
<Root/>
"""

        # act
        xmlContent = XmlSerializer.serialize(rootObject, Root, XmlSerializer.Formatting.Pretty)

        # assert
        self.assertEqual(xmlContent, expectedContent)

    def test_serialize_ShouldSerializeObjectWithSngleAttribute(self):
        # arrange
        rootObject = RootWithSingleAttribute()
        rootObject.id = 1
        expectedContent = """<?xml version="1.0" ?>
<RootWithSingleAttribute id="1"/>
"""

        # act
        xmlContent = XmlSerializer.serialize(rootObject, RootWithSingleAttribute, XmlSerializer.Formatting.Pretty)

        # assert
        self.assertEqual(xmlContent, expectedContent)

    def test_serialize_ShouldSerializeObjectWithManyAttributesOfDifferentTypes(self):
        # arrange
        rootObject = RootWithManyAttributes()
        rootObject.id = 57
        rootObject.name = 'root'
        rootObject.fitness = 14.72
        rootObject.color = Color('Red')
        expectedContent = """<?xml version="1.0" ?>
<RootWithManyAttributes color="Red" fitness="14.72" id="57" name="root"/>
"""

        # act
        xmlContent = XmlSerializer.serialize(rootObject, RootWithManyAttributes, XmlSerializer.Formatting.Pretty)

        # assert
        self.assertEqual(xmlContent, expectedContent)

    def test_serialize_ShouldSerializeObjectWithSingleValue(self):
        # arrange
        rootObject = RootWithSingleValue()
        rootObject.id = 4
        expectedContent = """<?xml version="1.0" ?>
<RootWithSingleValue>
\t<id>4</id>
</RootWithSingleValue>
"""

        # act
        xmlContent = XmlSerializer.serialize(rootObject, RootWithSingleValue, XmlSerializer.Formatting.Pretty)

        # assert
        self.assertEqual(xmlContent, expectedContent)

    def test_serialize_ShouldSerializeObjectWithManyValuesOfDifferentTypes(self):
        # arrange
        rootObject = RootWithManyValues()
        rootObject.id = 7
        rootObject.name = "XmlElement"
        rootObject.fitness = 3.14
        rootObject.color = Color("Green")
        expectedContent = """<?xml version="1.0" ?>
<RootWithManyValues>
\t<color>Green</color>
\t<fitness>3.14</fitness>
\t<id>7</id>
\t<name>XmlElement</name>
</RootWithManyValues>
"""

        # act
        xmlContent = XmlSerializer.serialize(rootObject, RootWithManyValues, XmlSerializer.Formatting.Pretty)

        # assert
        self.assertEqual(xmlContent, expectedContent)

    def test_serialize_ShouldSerializeObjectWithSingleChild(self):
        # arrange
        rootObject = RootWithChild()
        rootObject.child = (Child(),)
        expectedContent = """<?xml version="1.0" ?>
<RootWithChild>
\t<Child/>
</RootWithChild>
"""

        # act
        xmlContent = XmlSerializer.serialize(rootObject, RootWithChild, XmlSerializer.Formatting.Pretty)

        # assert
        self.assertEqual(xmlContent, expectedContent)

    def test_serialize_ShouldSerializeObjectWithManyDifferentChildren(self):
        # arrange
        rootObject = RootWithManyChildren()
        rootObject.child1 = (Child1(),)
        rootObject.child2 = (Child2(),)
        rootObject.child3 = (Child3(),)
        expectedContent = """<?xml version="1.0" ?>
<RootWithManyChildren>
\t<Child1/>
\t<Child2/>
\t<Child3/>
</RootWithManyChildren>
"""

        # act
        xmlContent = XmlSerializer.serialize(rootObject, RootWithManyChildren, XmlSerializer.Formatting.Pretty)

        # assert
        self.assertEqual(xmlContent, expectedContent)

    def test_serialize_ShouldSerializeObjectWithCollectionOfChildObjects(self):
        # arrange
        rootObject = RootWithChild()
        rootObject.child = (Child(), Child(), Child())
        expectedContent = """<?xml version="1.0" ?>
<RootWithChild>
\t<Child/>
\t<Child/>
\t<Child/>
</RootWithChild>
"""

        # act
        xmlContent = XmlSerializer.serialize(rootObject, RootWithChild, XmlSerializer.Formatting.Pretty)

        # assert
        self.assertEqual(xmlContent, expectedContent)

    def test_serialize_ShouldSerializeObjectWithNestedChildren(self):
        # arrange
        rootObject = RootWithNestedChildren()
        firstLevelChild = FirstLevelChild()
        secondLevelChild = SecondLevelChild()
        thirdLevelChild = ThirdLevelChild()
        secondLevelChild.child = (thirdLevelChild,)
        firstLevelChild.child = (secondLevelChild,)
        rootObject.child = (firstLevelChild,)
        expectedContent = """<?xml version="1.0" ?>
<RootWithNestedChildren>
\t<FirstLevelChild>
\t\t<SecondLevelChild>
\t\t\t<ThirdLevelChild/>
\t\t</SecondLevelChild>
\t</FirstLevelChild>
</RootWithNestedChildren>
"""

        # act
        xmlContent = XmlSerializer.serialize(rootObject, RootWithNestedChildren, XmlSerializer.Formatting.Pretty)

        # assert
        self.assertEqual(xmlContent, expectedContent)

    def test_serialize_ShouldSerializeComplexObject(self):
        # arrange
        rootObject = Module()
        rootObject.name = 'Arithmetic'
        rootObject.description = 'Set of classes targeting common arithmetical problems.'
        rootObject.classes = (Class(), Class(), Class())
        rootObject.classes[0].name = 'Number'
        rootObject.classes[0].accessModifier = 'public'
        rootObject.classes[0].implementedInterfaces = 'Comparable, Serializable, Cloneable'
        rootObject.classes[0].methods = (Method(), Method())
        rootObject.classes[0].methods[0].name = 'Add'
        rootObject.classes[0].methods[0].accessModifier = 'public'
        rootObject.classes[0].methods[0].argsNumber = 2
        rootObject.classes[0].methods[0].argsNames = 'number1, number2'
        rootObject.classes[0].methods[1].name = 'Subtract'
        rootObject.classes[0].methods[1].accessModifier = 'public'
        rootObject.classes[0].methods[1].argsNumber = 2
        rootObject.classes[0].methods[1].argsNames = 'number1, number2'
        rootObject.classes[1].name = 'Complex'
        rootObject.classes[1].accessModifier = 'public'
        rootObject.classes[1].implementedInterfaces = 'Comparable, Serializable, Cloneable'
        rootObject.classes[1].methods = (Method(),)
        rootObject.classes[1].methods[0].name = 'AsExponential'
        rootObject.classes[1].methods[0].accessModifier = 'public'
        rootObject.classes[1].methods[0].argsNumber = 0
        rootObject.classes[1].methods[0].argsNames = ""
        rootObject.classes[2].name = 'NaN'
        rootObject.classes[2].accessModifier = 'internal'
        rootObject.classes[2].implementedInterfaces = 'Comparable'
        rootObject.classes[2].methods = ()

        expectedContent = """<?xml version="1.0" ?>
<Module name="Arithmetic">
\t<description>Set of classes targeting common arithmetical problems.</description>
\t<Class accessModifier="public" name="Number">
\t\t<implementedInterfaces>Comparable, Serializable, Cloneable</implementedInterfaces>
\t\t<Method accessModifier="public" name="Add">
\t\t\t<argsNames>number1, number2</argsNames>
\t\t\t<argsNumber>2</argsNumber>
\t\t</Method>
\t\t<Method accessModifier="public" name="Subtract">
\t\t\t<argsNames>number1, number2</argsNames>
\t\t\t<argsNumber>2</argsNumber>
\t\t</Method>
\t</Class>
\t<Class accessModifier="public" name="Complex">
\t\t<implementedInterfaces>Comparable, Serializable, Cloneable</implementedInterfaces>
\t\t<Method accessModifier="public" name="AsExponential">
\t\t\t<argsNames/>
\t\t\t<argsNumber>0</argsNumber>
\t\t</Method>
\t</Class>
\t<Class accessModifier="internal" name="NaN">
\t\t<implementedInterfaces>Comparable</implementedInterfaces>
\t</Class>
</Module>
"""

        # act
        xmlContent = XmlSerializer.serialize(rootObject, Module, XmlSerializer.Formatting.Pretty)

        # assert
        self.assertEqual(xmlContent, expectedContent)

    def test_serialize_ShouldSerializeObjectWithChildAsSingleCollection(self):
        # arrange
        rootObject = RootWithSingleChild()
        rootObject.child = Child()
        expectedContent = """<?xml version="1.0" ?>
<RootWithSingleChild>
\t<Child/>
</RootWithSingleChild>
"""

        # act
        xmlContent = XmlSerializer.serialize(rootObject, RootWithSingleChild, XmlSerializer.Formatting.Pretty)

        # assert
        self.assertEqual(xmlContent, expectedContent)

    def test_serialize_ShouldSerializeObjectWithComplexChildrenAsSingleCollection(self):
        # arrange
        method = Method()
        method.name = 'Add'
        method.accessModifier = 'public'
        method.argsNumber = 2
        method.argsNames = 'number1, number2'

        methodChild = MethodChild()
        methodChild.method = method

        childChild = ChildChild()
        childChild.child = Child()

        rootObject = RootWithSingleCollectionChildren()
        rootObject.methodChild = methodChild
        rootObject.childChild = childChild
        expectedContent = """<?xml version="1.0" ?>
<RootWithSingleCollectionChildren>
\t<ChildChild>
\t\t<Child/>
\t</ChildChild>
\t<MethodChild>
\t\t<Method accessModifier="public" name="Add">
\t\t\t<argsNames>number1, number2</argsNames>
\t\t\t<argsNumber>2</argsNumber>
\t\t</Method>
\t</MethodChild>
</RootWithSingleCollectionChildren>
"""

        # act
        xmlContent = XmlSerializer.serialize(rootObject, RootWithSingleCollectionChildren,
                                             XmlSerializer.Formatting.Pretty)

        # assert
        self.assertEqual(xmlContent, expectedContent)

    def test_serialize_ShouldSerializeObjectWithConstructor(self):
        # arrange
        rootObject = RootWithConstructor()
        rootObject.string = "different string"
        rootObject.integer = 15
        rootObject.object = Child()
        for i in range(0, 3):
            rootObject.objects += (Child1(),)

        expectedContent = """<?xml version="1.0" ?>
<RootWithConstructor>
\t<integer>15</integer>
\t<string>different string</string>
\t<Child/>
\t<Child1/>
\t<Child1/>
\t<Child1/>
</RootWithConstructor>
"""

        # act
        xmlContent = XmlSerializer.serialize(rootObject, RootWithConstructor, XmlSerializer.Formatting.Pretty)

        # assert
        self.assertEqual(xmlContent, expectedContent)
