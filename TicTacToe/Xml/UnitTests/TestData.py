from Xml.XmlSerializer import XmlAttribute, XmlValue, XmlCollection


class Root:
    pass


class RootWithSingleAttribute:
    id = XmlAttribute(int)


class Color:
    def __init__(self, string):
        self.string = string

    def __str__(self):
        return self.string


def colorConv(string):
    return Color(string)


class RootWithManyAttributes:
    id = XmlAttribute(int)
    name = XmlAttribute(str)
    fitness = XmlAttribute(float)
    color = XmlAttribute(colorConv)


class RootWithSingleValue:
    id = XmlValue(int)


class RootWithManyValues:
    id = XmlValue(int)
    name = XmlValue(str)
    fitness = XmlValue(float)
    color = XmlValue(colorConv)


class Child:
    pass


class RootWithChild:
    child = XmlCollection(Child)


class Child1:
    pass


class Child2:
    pass


class Child3:
    pass


class RootWithManyChildren:
    child1 = XmlCollection(Child1)
    child2 = XmlCollection(Child2)
    child3 = XmlCollection(Child3)


class ThirdLevelChild:
    pass


class SecondLevelChild:
    child = XmlCollection(ThirdLevelChild)


class FirstLevelChild:
    child = XmlCollection(SecondLevelChild)


class RootWithNestedChildren:
    child = XmlCollection(FirstLevelChild)


class Method:
    name = XmlAttribute(str)
    accessModifier = XmlAttribute(str)
    argsNumber = XmlValue(int)
    argsNames = XmlValue(str)


class Class:
    name = XmlAttribute(str)
    accessModifier = XmlAttribute(str)
    implementedInterfaces = XmlValue(str)
    methods = XmlCollection(Method)


class Module:
    name = XmlAttribute(str)
    description = XmlValue(str)
    classes = XmlCollection(Class)


class RootWithSingleChild:
    child = XmlCollection(Child, True)


class MethodChild:
    method = XmlCollection(Method, True)


class ChildChild:
    child = XmlCollection(Child, True)


class RootWithSingleCollectionChildren:
    methodChild = XmlCollection(MethodChild, True)
    childChild = XmlCollection(ChildChild, True)


class RootWithConstructor:
    string = XmlValue(str)
    integer = XmlValue(int)
    object = XmlCollection(Child, True)
    objects = XmlCollection(Child1)

    def __init__(self):
        self.string = "string"
        self.integer = 12
        self.object = None
        self.objects = ()
