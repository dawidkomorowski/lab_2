from Game.GameField import GameOField, GameXField
from Game.GamePlayer import GameOPlayer, GameXPlayer, UnknownGamePlayerTypeException


class GameFieldTypeMapper:
    def fromGamePlayer(self, gamePlayer):
        if gamePlayer is GameOPlayer:
            return GameOField
        if gamePlayer is GameXPlayer:
            return GameXField
        raise UnknownGamePlayerTypeException(gamePlayer)

    def toGamePlayer(self, gameField):
        if gameField is GameOField:
            return GameOPlayer
        if gameField is GameXField:
            return GameXPlayer
        raise NoSuitableGamePlayerTypeForGameFieldException(gameField)


class NoSuitableGamePlayerTypeForGameFieldException(Exception):
    def __init__(self, gameField=""):
        self.message = "No suitable game player type for game field: " + str(gameField)
