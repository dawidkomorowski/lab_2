from unittest import TestCase

from Game.GameField import GameOField, GameXField
from Game.GameFieldTypeMapper import GameFieldTypeMapper
from Game.GamePlayer import GameOPlayer, GameXPlayer


class TestGameFieldTypeMapper(TestCase):
    def test_fromGamePlayer(self):
        # arrange
        gameFieldTypeMapper = GameFieldTypeMapper()

        # act
        gameFieldType1 = gameFieldTypeMapper.fromGamePlayer(GameOPlayer)
        gameFieldType2 = gameFieldTypeMapper.fromGamePlayer(GameXPlayer)

        # assert
        self.assertEqual(gameFieldType1, GameOField)
        self.assertEqual(gameFieldType2, GameXField)

    def test_toGamePlayer(self):
        # arrange
        gameFieldTypeMapper = GameFieldTypeMapper()

        # act
        gamePlayerType1 = gameFieldTypeMapper.toGamePlayer(GameOField)
        gamePlayerType2 = gameFieldTypeMapper.toGamePlayer(GameXField)

        # assert
        self.assertEqual(gamePlayerType1, GameOPlayer)
        self.assertEqual(gamePlayerType2, GameXPlayer)
