from unittest import TestCase

from Game.GameField import GameEmptyField, GameOField, GameXField
from Game.GameState import GameState


class TestGameState(TestCase):
    def test_getFieldAt(self):
        # arrange
        gameState = GameState()
        gameState.map = [[GameOField, GameXField, GameOField],
                         [GameXField, GameEmptyField, GameEmptyField],
                         [GameEmptyField, GameOField, GameXField]]

        # act
        field1 = gameState.getFieldAt(0, 0)
        field2 = gameState.getFieldAt(1, 1)
        field3 = gameState.getFieldAt(2, 2)

        # assert
        self.assertEqual(field1, GameOField)
        self.assertEqual(field2, GameEmptyField)
        self.assertEqual(field3, GameXField)

    def test_setFieldAt(self):
        # arrange
        gameState = GameState()
        gameState.map = [[GameOField, GameXField, GameOField],
                         [GameXField, GameEmptyField, GameEmptyField],
                         [GameEmptyField, GameOField, GameXField]]

        field1 = GameEmptyField
        field2 = GameXField
        field3 = GameOField

        # act
        gameState.setFieldAt(0, 0, field1)
        gameState.setFieldAt(1, 1, field2)
        gameState.setFieldAt(2, 2, field3)

        # assert
        self.assertEqual(gameState.getFieldAt(0, 0), field1)
        self.assertEqual(gameState.getFieldAt(1, 1), field2)
        self.assertEqual(gameState.getFieldAt(2, 2), field3)
