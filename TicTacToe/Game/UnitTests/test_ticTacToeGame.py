from unittest import TestCase

from Game.GameEngine import GameEngine
from Game.GameField import GameEmptyField
from Game.GameFieldTypeMapper import GameFieldTypeMapper
from Game.GameMove import GameMove
from Game.GameRules.AfterMovePerspective.DrawGameRule import DrawGameRuleException
from Game.GameRules.AfterMovePerspective.PlayerWonGameRule import PlayerWonGameRuleException
from Game.GameRules.BeforeMovePerspective.ValidateMoveTargetPositionGameRule import \
    InvalidTargetPositionGameRuleException
from Game.GameRulesEngine.GameRuleContextBuilder import GameRuleContextBuilder
from Game.GameRulesEngine.GameRulePerspectiveConfiguration import GameRulePerspectiveConfiguration
from Game.GameRulesEngine.GameRulePerspectiveConfigurationBuilder import GameRulePerspectiveConfigurationBuilder
from Game.GameRulesEngine.GameRulesEngine import GameRulesEngine
from Game.GameState import GameState
from Game.TicTacToeGame import TicTacToeGame


class TestTicTacToeGame(TestCase):
    def test_GameShouldBeInitialized_JustAfterCreation(self):
        # arrange
        # act
        ticTacToeGame = TicTacToeGame()

        # assert
        self.assertIsInstance(ticTacToeGame._gameFieldTypeMapper, GameFieldTypeMapper)
        self.assertIsInstance(ticTacToeGame._gameRulesEngine, GameRulesEngine)
        self.assertIsInstance(ticTacToeGame._gameRuleContextBulder, GameRuleContextBuilder)
        self.assertIsInstance(ticTacToeGame._gameState, GameState)
        self.assertIsInstance(ticTacToeGame._gameRulePerspectiveConfigurationBuilder,
                              GameRulePerspectiveConfigurationBuilder)
        self.assertIsInstance(ticTacToeGame._gameRulePerspectiveConfiguration, GameRulePerspectiveConfiguration)
        self.assertIsInstance(ticTacToeGame._gameEngine, GameEngine)
        self.assertTrue(self.isGameMapEmpty(ticTacToeGame._gameState))

    def test_GameShouldBeInitialized_WhenResetIsCalled(self):
        # arrange
        ticTacToeGame = TicTacToeGame()

        # act
        ticTacToeGame.reset()

        # assert
        self.assertIsInstance(ticTacToeGame._gameFieldTypeMapper, GameFieldTypeMapper)
        self.assertIsInstance(ticTacToeGame._gameRulesEngine, GameRulesEngine)
        self.assertIsInstance(ticTacToeGame._gameRuleContextBulder, GameRuleContextBuilder)
        self.assertIsInstance(ticTacToeGame._gameState, GameState)
        self.assertIsInstance(ticTacToeGame._gameRulePerspectiveConfigurationBuilder,
                              GameRulePerspectiveConfigurationBuilder)
        self.assertIsInstance(ticTacToeGame._gameRulePerspectiveConfiguration, GameRulePerspectiveConfiguration)
        self.assertIsInstance(ticTacToeGame._gameEngine, GameEngine)
        self.assertTrue(self.isGameMapEmpty(ticTacToeGame._gameState))

    def isGameMapEmpty(self, gameState):
        for x in range(0, 2):
            for y in range(0, 2):
                if not gameState.getFieldAt(x, y) is GameEmptyField:
                    return False
        return True

    def test_MakeMoveShouldReturnNonesAndPlayerWon(self):
        # arrange
        ticTacToeGame = TicTacToeGame()

        # act
        none1 = ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (0, 0)))
        none2 = ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (2, 2)))
        none3 = ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (1, 0)))
        none4 = ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (2, 0)))
        none5 = ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (2, 1)))
        none6 = ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (1, 1)))
        none7 = ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (0, 1)))
        playerWon = ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (0, 2)))

        # assert
        self.assertIs(none1, None)
        self.assertIs(none2, None)
        self.assertIs(none3, None)
        self.assertIs(none4, None)
        self.assertIs(none5, None)
        self.assertIs(none6, None)
        self.assertIs(none7, None)
        self.assertIsInstance(playerWon, PlayerWonGameRuleException)

    def test_GameShouldBeReinitialized_WhenResetIsCalled(self):
        # arrange
        ticTacToeGame = TicTacToeGame()

        ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (0, 0)))
        ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (2, 2)))
        ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (1, 0)))
        ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (2, 0)))
        ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (2, 1)))
        ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (1, 1)))
        ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (0, 1)))
        ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (0, 2)))

        # act
        ticTacToeGame.reset()

        # assert
        self.assertIsInstance(ticTacToeGame._gameFieldTypeMapper, GameFieldTypeMapper)
        self.assertIsInstance(ticTacToeGame._gameRulesEngine, GameRulesEngine)
        self.assertIsInstance(ticTacToeGame._gameRuleContextBulder, GameRuleContextBuilder)
        self.assertIsInstance(ticTacToeGame._gameState, GameState)
        self.assertIsInstance(ticTacToeGame._gameRulePerspectiveConfigurationBuilder,
                              GameRulePerspectiveConfigurationBuilder)
        self.assertIsInstance(ticTacToeGame._gameRulePerspectiveConfiguration, GameRulePerspectiveConfiguration)
        self.assertIsInstance(ticTacToeGame._gameEngine, GameEngine)
        self.assertTrue(self.isGameMapEmpty(ticTacToeGame._gameState))

    def test_MakeMoveShouldReturnNonesAndDraw(self):
        # arrange
        ticTacToeGame = TicTacToeGame()

        # act
        none1 = ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (1, 1)))
        none2 = ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (0, 0)))
        none3 = ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (2, 0)))
        none4 = ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (0, 2)))
        none5 = ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (0, 1)))
        none6 = ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (2, 1)))
        none7 = ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (1, 2)))
        none8 = ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (1, 0)))
        draw = ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (2, 2)))

        # assert
        self.assertIs(none1, None)
        self.assertIs(none2, None)
        self.assertIs(none3, None)
        self.assertIs(none4, None)
        self.assertIs(none5, None)
        self.assertIs(none6, None)
        self.assertIs(none7, None)
        self.assertIs(none8, None)
        self.assertIsInstance(draw, DrawGameRuleException)

    def test_MakeMoveShouldReturnNoneAndInvalidTargetPositionAndPlayerWon(self):
        # arrange
        ticTacToeGame = TicTacToeGame()

        ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (1, 1)))
        ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (0, 0)))
        ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (2, 0)))
        ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (0, 2)))
        ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (0, 1)))
        ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (2, 1)))
        ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (1, 2)))

        # act
        none = ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (2, 2)))
        invalidTargetPosition = ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (2, 0)))
        playerWon = ticTacToeGame.makeMove(GameMove(ticTacToeGame._gameState.currentPlayer, (1, 0)))

        # assert
        self.assertIs(none, None)
        self.assertIsInstance(invalidTargetPosition, InvalidTargetPositionGameRuleException)
        self.assertIsInstance(playerWon, PlayerWonGameRuleException)
