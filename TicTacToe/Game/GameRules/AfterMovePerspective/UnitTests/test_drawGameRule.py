from unittest import TestCase

from Game.GameField import GameOField, GameXField, GameEmptyField
from Game.GameFieldTypeMapper import GameFieldTypeMapper
from Game.GameMove import GameMove
from Game.GamePlayer import GameXPlayer
from Game.GameRules.AfterMovePerspective.DrawGameRule import DrawGameRule, DrawGameRuleException
from Game.GameRulesEngine.GameRuleContextBuilder import GameRuleContextBuilder
from Game.GameState import GameState


class TestDrawGameRule(TestCase):
    def test_ShouldNotRaiseException_UntilEmptyFieldsPresent(self):
        # arrange
        gameFieldTypeMapper = GameFieldTypeMapper()
        gameRule = DrawGameRule(gameFieldTypeMapper)
        gameRuleContextBuilder = GameRuleContextBuilder()
        gameState = GameState(GameXPlayer)
        gameState.map = [[GameOField, GameXField, GameOField],
                         [GameXField, GameEmptyField, GameEmptyField],
                         [GameEmptyField, GameOField, GameXField]]
        gameMove = GameMove(GameXPlayer, (1, 1))

        gameRuleContext = gameRuleContextBuilder.context().withGameState(gameState).withGameMove(gameMove).build()

        # act
        # assert
        gameRule.run(gameRuleContext)

    def test_ShouldNotRaiseException_WhenWinnerPresent(self):
        # arrange
        gameFieldTypeMapper = GameFieldTypeMapper()
        gameRule = DrawGameRule(gameFieldTypeMapper)
        gameRuleContextBuilder = GameRuleContextBuilder()
        gameState = GameState(GameXPlayer)
        gameState.map = [[GameOField, GameXField, GameXField],
                         [GameXField, GameOField, GameXField],
                         [GameOField, GameOField, GameOField]]
        gameMove = GameMove(GameXPlayer, (1, 1))

        gameRuleContext = gameRuleContextBuilder.context().withGameState(gameState).withGameMove(gameMove).build()

        # act
        # assert
        gameRule.run(gameRuleContext)

    def test_ShouldRaiseException_WhenNeitherEmptyFieldsNorWinnerPresent(self):
        # arrange
        gameFieldTypeMapper = GameFieldTypeMapper()
        gameRule = DrawGameRule(gameFieldTypeMapper)
        gameRuleContextBuilder = GameRuleContextBuilder()
        gameState = GameState(GameXPlayer)
        gameState.map = [[GameOField, GameXField, GameXField],
                         [GameXField, GameOField, GameOField],
                         [GameOField, GameXField, GameXField]]
        gameMove = GameMove(GameXPlayer, (1, 1))

        gameRuleContext = gameRuleContextBuilder.context().withGameState(gameState).withGameMove(gameMove).build()

        # act
        # assert
        with self.assertRaises(DrawGameRuleException):
            gameRule.run(gameRuleContext)
