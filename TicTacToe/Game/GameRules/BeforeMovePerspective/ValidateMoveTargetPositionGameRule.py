from Game.GameField import GameEmptyField
from Game.GameRules.BeforeMovePerspective.BeforeMovePerspective import BeforeMovePerspective
from Game.GameRulesEngine.GameRule import GameRule, GameRuleException


class ValidateMoveTargetPositionGameRule(GameRule):
    def __init__(self):
        self.perspective = BeforeMovePerspective

    def run(self, context):
        (x, y) = context.gameMove.targetXYPosition
        if context.gameState.getFieldAt(x, y) != GameEmptyField:
            raise InvalidTargetPositionGameRuleException()


class InvalidTargetPositionGameRuleException(GameRuleException):
    pass
