from Game.GameRules.Common import Common
from Game.GameRules.MakeMovePerspective.MakeMovePerspective import MakeMovePerspective
from Game.GameRulesEngine.GameRule import GameRule


class MakeMoveGameRule(GameRule):
    def __init__(self, gameFieldTypeMapper):
        self.perspective = MakeMovePerspective
        self._gameFieldTypeMapper = gameFieldTypeMapper

    def run(self, context):
        (x, y) = context.gameMove.targetXYPosition
        gameField = self._gameFieldTypeMapper.fromGamePlayer(context.gameMove.gamePlayer)
        context.gameState.setFieldAt(x, y, gameField)
        context.gameState.currentPlayer = Common.oppositePlayer(context.gameState.currentPlayer)
