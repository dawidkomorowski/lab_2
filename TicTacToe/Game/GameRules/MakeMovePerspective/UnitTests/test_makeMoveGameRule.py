from unittest import TestCase

from Game.GameField import GameOField, GameEmptyField, GameXField
from Game.GameFieldTypeMapper import GameFieldTypeMapper
from Game.GameMove import GameMove
from Game.GamePlayer import GameXPlayer, GameOPlayer
from Game.GameRules.MakeMovePerspective.MakeMoveGameRule import MakeMoveGameRule
from Game.GameRulesEngine.GameRuleContextBuilder import GameRuleContextBuilder
from Game.GameState import GameState


class TestMakeMoveGameRule(TestCase):
    def test_ShouldCorrectlyApplySingleMove(self):
        # arrange
        gameFieldTypeMapper = GameFieldTypeMapper()
        gameRule = MakeMoveGameRule(gameFieldTypeMapper)
        gameRuleContextBuilder = GameRuleContextBuilder()
        gameState = GameState(GameXPlayer)
        gameMove = GameMove(GameOPlayer, (0, 0))

        gameRuleContext = gameRuleContextBuilder.context().withGameState(gameState).withGameMove(gameMove).build()

        expectedGameStateMap = [[GameOField, GameEmptyField, GameEmptyField],
                                [GameEmptyField, GameEmptyField, GameEmptyField],
                                [GameEmptyField, GameEmptyField, GameEmptyField]]

        # act
        gameRule.run(gameRuleContext)

        # assert
        self.assertEqual(gameState.map, expectedGameStateMap)

    def test_ShouldCorrectlyApplyMultipleMoves(self):
        # arrange
        gameFieldTypeMapper = GameFieldTypeMapper()
        gameRule = MakeMoveGameRule(gameFieldTypeMapper)
        gameRuleContextBuilder = GameRuleContextBuilder()
        gameState = GameState(GameXPlayer)

        expectedGameStateMap = [[GameOField, GameOField, GameXField],
                                [GameEmptyField, GameXField, GameEmptyField],
                                [GameOField, GameEmptyField, GameEmptyField]]

        # act
        gameRuleContext = gameRuleContextBuilder.context().withGameState(gameState).withGameMove(
                GameMove(GameOPlayer, (0, 0))).build()
        gameRule.run(gameRuleContext)

        gameRuleContext = gameRuleContextBuilder.context().withGameState(gameState).withGameMove(
                GameMove(GameXPlayer, (1, 1))).build()
        gameRule.run(gameRuleContext)

        gameRuleContext = gameRuleContextBuilder.context().withGameState(gameState).withGameMove(
                GameMove(GameOPlayer, (1, 0))).build()
        gameRule.run(gameRuleContext)

        gameRuleContext = gameRuleContextBuilder.context().withGameState(gameState).withGameMove(
                GameMove(GameXPlayer, (2, 0))).build()
        gameRule.run(gameRuleContext)

        gameRuleContext = gameRuleContextBuilder.context().withGameState(gameState).withGameMove(
                GameMove(GameOPlayer, (0, 2))).build()
        gameRule.run(gameRuleContext)

        # assert
        self.assertEqual(gameState.map, expectedGameStateMap)

    def test_ShouldChangeCurrentPlayerScenarioOne_WhenMoveApplied(self):
        # arrange
        gameFieldTypeMapper = GameFieldTypeMapper()
        gameRule = MakeMoveGameRule(gameFieldTypeMapper)
        gameRuleContextBuilder = GameRuleContextBuilder()
        gameState = GameState(GameXPlayer)
        gameMove = GameMove(GameXPlayer, (0, 0))

        gameRuleContext = gameRuleContextBuilder.context().withGameState(gameState).withGameMove(gameMove).build()

        # act
        gameRule.run(gameRuleContext)

        # assert
        self.assertEqual(gameState.currentPlayer, GameOPlayer)

    def test_ShouldChangeCurrentPlayerScenarioTwo_WhenMoveApplied(self):
        # arrange
        gameFieldTypeMapper = GameFieldTypeMapper()
        gameRule = MakeMoveGameRule(gameFieldTypeMapper)
        gameRuleContextBuilder = GameRuleContextBuilder()
        gameState = GameState(GameOPlayer)
        gameMove = GameMove(GameOPlayer, (0, 0))

        gameRuleContext = gameRuleContextBuilder.context().withGameState(gameState).withGameMove(gameMove).build()

        # act
        gameRule.run(gameRuleContext)

        # assert
        self.assertEqual(gameState.currentPlayer, GameXPlayer)
