from Game.GameRulesEngine.GameRulePerspectiveConfiguration import GameRulePerspectiveConfiguration


class GameRulePerspectiveConfigurationBuilder:
    def __init__(self):
        self._perspectives = ()

    def configuration(self):
        self._perspectives = ()
        return self

    def withPerspective(self, perspective):
        self._perspectives = self._perspectives + (perspective,)
        return self

    def build(self):
        return GameRulePerspectiveConfiguration(self._perspectives)
