from unittest import TestCase

from Game.GameRules.AfterMovePerspective.AfterMovePerspective import AfterMovePerspective
from Game.GameRules.BeforeMovePerspective.BeforeMovePerspective import BeforeMovePerspective
from Game.GameRules.MakeMovePerspective.MakeMovePerspective import MakeMovePerspective
from Game.GameRulesEngine.GameRulePerspectiveConfigurationBuilder import GameRulePerspectiveConfigurationBuilder


class TestGameRulePerspectiveConfigurationBuilder(TestCase):
    def test_ShouldReturnEmptyContextBuilder(self):
        # arrange
        gameRulePerspectiveConfigurationBuilder = GameRulePerspectiveConfigurationBuilder()

        # act
        emptyGameRulePerspectiveConfigurationBuilder = gameRulePerspectiveConfigurationBuilder.configuration()

        # assert
        self.assertEqual(emptyGameRulePerspectiveConfigurationBuilder, gameRulePerspectiveConfigurationBuilder)
        self.assertEqual(emptyGameRulePerspectiveConfigurationBuilder._perspectives, ())

    def test_ShouldBuildEmptyConfiguration(self):
        # arrange
        gameRulePerspectiveConfigurationBuilder = GameRulePerspectiveConfigurationBuilder()

        # act
        gameRulePerspectiveConfiguration = gameRulePerspectiveConfigurationBuilder.configuration().build()

        # assert
        self.assertEqual(gameRulePerspectiveConfigurationBuilder._perspectives, ())

    def test_ShouldBuildConfigurationWithSinglePerspective(self):
        # arrange
        gameRulePerspectiveConfigurationBuilder = GameRulePerspectiveConfigurationBuilder()

        # act
        gameRulePerspectiveConfiguration = gameRulePerspectiveConfigurationBuilder.configuration().withPerspective(
                BeforeMovePerspective).build()

        # assert
        self.assertEqual(gameRulePerspectiveConfigurationBuilder._perspectives, (BeforeMovePerspective,))

    def test_ShouldBuildConfigurationWithMultiplePerspectives(self):
        # arrange
        gameRulePerspectiveConfigurationBuilder = GameRulePerspectiveConfigurationBuilder()

        # act
        gameRulePerspectiveConfiguration = gameRulePerspectiveConfigurationBuilder.configuration().withPerspective(
                BeforeMovePerspective).withPerspective(MakeMovePerspective).withPerspective(
                AfterMovePerspective).build()

        # assert
        self.assertEqual(gameRulePerspectiveConfigurationBuilder._perspectives,
                         (BeforeMovePerspective, MakeMovePerspective, AfterMovePerspective))
