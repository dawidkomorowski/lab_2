from abc import abstractmethod


class AbstractGame:
    @abstractmethod
    def reset(self):
        pass

    @abstractmethod
    def makeMove(self, gameMove):
        pass
