import logging

from Common.PlayerHelper import PlayerHelper
from Common.RequestHeaders import RequestHeaders
from Common.RequestHelper import RequestHelper
from Common.ResponseHelper import ResponseHelper
from Common.ResponseTypes import ResponseTypes
from Game.GameRules.AfterMovePerspective.DrawGameRule import DrawGameRuleException
from Game.GameRules.AfterMovePerspective.PlayerWonGameRule import PlayerWonGameRuleException
from Game.TicTacToeGame import TicTacToeGame
from Network.RRServer import RRServer, TimeoutExceededException
from UserInterface.Common import Common
from UserInterface.CurrentPlayerCliOutputComponent import CurrentPlayerCliOutputComponent
from UserInterface.GameMessageCliOutputComponent import GameMessageCliOutputComponent
from UserInterface.GameMessageCliOutputComponentCreator import GameMessageCliOutputComponentCreator
from UserInterface.GameMoveCliInputComponent import GameMoveCliInputComponent, IncorrectGameMoveInputException
from UserInterface.GameStateCliOutputComponent import GameStateCliOutputComponent
from UserInterface.InputOutput.Readers.ConsoleReader import ConsoleReader
from UserInterface.InputOutput.Writers.ConsoleWriter import ConsoleWriter
from UserInterface.InputOutput.Writers.StringWriter import StringWriter


class TicTacToeGameServer:
    logger = logging.getLogger(__name__)

    def __init__(self, configuration):
        self.logger.debug("Initializing game server.")

        self._server = RRServer(configuration)
        self._reader = ConsoleReader()
        self._writer = ConsoleWriter()
        self._internalWriter = StringWriter()
        self._game = TicTacToeGame()

        self._gameStateCliOutputComponent = GameStateCliOutputComponent(self._game._gameState)
        self._currentPlayerCliOutputComponent = CurrentPlayerCliOutputComponent(self._game._gameState)
        self._inputDescriptionCliOutputComponent = GameMessageCliOutputComponent(
                "\nEnter number of field you want to place o/x.")
        self._incorrectInputCliOutputComponent = GameMessageCliOutputComponent(
                "Incorrect input given. Please enter correct input.\n")
        self._gameMoveCliInputComponent = GameMoveCliInputComponent()

        self._gameMessageCliOutputComponentCreator = GameMessageCliOutputComponentCreator()

    def run(self):
        self.logger.debug("Starting game server.")

        try:
            self._server.start()

            localPlayer = PlayerHelper.nextRandomPlayer()
            remotePlayer = PlayerHelper.oppositePlayer(localPlayer)
            self._game._gameState.currentPlayer = PlayerHelper.nextRandomPlayer()

            # write player assignment message to local and remote
            self._writer.write("You are " + Common.playerToString(localPlayer) + "\n")
            request = RequestHelper.createRequest(RequestHeaders.DisplayPlainText,
                                                  "You are " + Common.playerToString(remotePlayer) + "\n")
            self._server.sendRequest(request)

            while True:
                # write current player message to local and remote
                self._internalWriter.output = ""
                self._currentPlayerCliOutputComponent.write(self._internalWriter)
                self._writer.write(self._internalWriter.output)
                request = RequestHelper.createRequest(RequestHeaders.DisplayPlainText, self._internalWriter.output)
                self._server.sendRequest(request)

                # write game state message to local and remote
                self._gameStateCliOutputComponent.write(self._writer)
                request = RequestHelper.createRequest(RequestHeaders.DisplayGameState, self._game._gameState)
                self._server.sendRequest(request)

                currentPlayer = self._game._gameState.currentPlayer
                while currentPlayer == self._game._gameState.currentPlayer:
                    if self._game._gameState.currentPlayer == localPlayer:
                        gameMove = None
                        while gameMove is None:
                            try:
                                # write ask for input message to local
                                self._inputDescriptionCliOutputComponent.write(self._writer)
                                # read input from local
                                self._gameMoveCliInputComponent.read(self._reader)
                                gameMove = self._gameMoveCliInputComponent.lastGameMove
                            except IncorrectGameMoveInputException:
                                # write incorrect input message to local
                                self._incorrectInputCliOutputComponent.write(self._writer)

                    if self._game._gameState.currentPlayer == remotePlayer:
                        # ask for input remote
                        request = RequestHelper.createRequest(RequestHeaders.AskForGameMove)
                        response = self._server.sendRequest(request)
                        gameMove = ResponseHelper.extractData(response, ResponseTypes.GameMoveResponse)

                    gameMove.gamePlayer = self._game._gameState.currentPlayer
                    gameRuleException = self._game.makeMove(gameMove)

                    # write move result to buffer
                    gameMessageCliOutputComponent = self._gameMessageCliOutputComponentCreator.createGameMessage(
                            gameRuleException)
                    self._internalWriter.output = ""
                    gameMessageCliOutputComponent.write(self._internalWriter)

                    if PlayerHelper.isMessageForPlayer(localPlayer, currentPlayer, gameRuleException):
                        # write new line local
                        self._writer.write("")

                        # write move result local
                        self._writer.write(self._internalWriter.output)

                    if PlayerHelper.isMessageForPlayer(remotePlayer, currentPlayer, gameRuleException):
                        # write new line remote
                        request = RequestHelper.createRequest(RequestHeaders.DisplayPlainText, " ")
                        self._server.sendRequest(request)

                        # write move result remote
                        request = RequestHelper.createRequest(RequestHeaders.DisplayPlainText,
                                                              self._internalWriter.output)
                        self._server.sendRequest(request)

                # write new line local and remote
                self._writer.write("")
                request = RequestHelper.createRequest(RequestHeaders.DisplayPlainText, " ")
                self._server.sendRequest(request)

                if isinstance(gameRuleException, PlayerWonGameRuleException) or isinstance(gameRuleException,
                                                                                           DrawGameRuleException):
                    # write game state message to local and remote
                    self._gameStateCliOutputComponent.write(self._writer)
                    request = RequestHelper.createRequest(RequestHeaders.DisplayGameState, self._game._gameState)
                    self._server.sendRequest(request)
                    break

            # inform remote about end of game session
            request = RequestHelper.createRequest(RequestHeaders.EndGameSession)
            self._server.sendRequest(request)

            self._server.stop()
        except TimeoutExceededException as ex:
            self._server.stop()
            raise ex

        self.logger.debug("Closing game server.")
