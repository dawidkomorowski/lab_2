from Network.Core.ReceivedHandler import ReceivedHandler


class ResponseReceivedHandler(ReceivedHandler):
    def __init__(self, rrServer):
        self._rrServer = rrServer

    def handle(self, data):
        self._rrServer._receivedData.put(data)
