from Network.Core.ReceivedHandler import ReceivedHandler


class RequestReceivedHandler(ReceivedHandler):
    def __init__(self, rrClient):
        self._rrClient = rrClient

    def handle(self, data):
        self._rrClient._receivedData.put(data)
