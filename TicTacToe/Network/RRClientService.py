from abc import abstractmethod


class RRClientService:
    @abstractmethod
    def serve(self, request):
        pass

    @abstractmethod
    def available(self):
        pass
