from Network.Core.ReceivedHandler import ReceivedHandler


class TestReceivedHandler(ReceivedHandler):
    def __init__(self):
        self.lastReceivedData = None
        self.receivedDataTuple = ()

    def handle(self, data):
        self.lastReceivedData = data
        self.receivedDataTuple += (data,)
