from unittest import TestCase

from Network.Core.Configuration import Configuration
from Xml.XmlSerializer import XmlSerializer


class TestConfiguration(TestCase):
    def test_ShouldSerializeAndDeserialize(self):
        # arrange
        configuration = Configuration()
        configuration.serverAddress = "192.168.135.172"
        configuration.port = 1234
        configuration.timeout = 1.23
        configuration.rrTimeout = 2.34
        configuration.connectTimeout = 123.456
        configuration.packetSize = 512

        # act
        serializedConfiguration = XmlSerializer.serialize(configuration, Configuration)
        deserializedConfiguration = XmlSerializer.deserialize(serializedConfiguration, Configuration)

        # assert
        self.assertIsInstance(deserializedConfiguration, Configuration)
        self.assertEqual(deserializedConfiguration.serverAddress, configuration.serverAddress)
        self.assertEqual(deserializedConfiguration.port, configuration.port)
        self.assertEqual(deserializedConfiguration.timeout, configuration.timeout)
        self.assertEqual(deserializedConfiguration.rrTimeout, configuration.rrTimeout)
        self.assertEqual(deserializedConfiguration.connectTimeout, configuration.connectTimeout)
        self.assertEqual(deserializedConfiguration.packetSize, configuration.packetSize)
