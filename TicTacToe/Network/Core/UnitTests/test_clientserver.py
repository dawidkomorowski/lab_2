from time import sleep
from unittest import TestCase

from Network.Core.Client import Client
from Network.Core.Server import Server
from Network.Core.UnitTests.TestData import TestReceivedHandler


class TestClientServer(TestCase):
    def test_ShouldClientConnectAndDisconnectKindly(self):
        # arrange
        client = Client()
        server = Server()

        # act
        # assert
        server.start()
        client.start()
        client.stop()
        server.stop()

    def test_ShouldServerReceive_WhenClientSend(self):
        # arrange
        client = Client()
        server = Server()
        server.receivedHandler = TestReceivedHandler()

        dataSent = "data"

        # act
        server.start()
        client.start()

        client.send(dataSent)
        sleep(0.1)

        client.stop()
        server.stop()

        dataReceived = server.receivedHandler.lastReceivedData

        # assert
        self.assertEqual(dataReceived, dataSent)

    def test_ShouldClientReceive_WhenServerSend(self):
        # arrange
        client = Client()
        server = Server()
        client.receivedHandler = TestReceivedHandler()

        dataSent = "data"

        # act
        server.start()
        client.start()

        server.send(dataSent)
        sleep(0.1)

        client.stop()
        server.stop()

        dataReceived = client.receivedHandler.lastReceivedData

        # assert
        self.assertEqual(dataReceived, dataSent)

    def test_ClientServerCommunicationScenario(self):
        # arrange
        client = Client()
        server = Server()
        client.receivedHandler = TestReceivedHandler()
        server.receivedHandler = TestReceivedHandler()

        dataFromClientToServer = ("Test", "data", "from", "client", "to", "server", ".")
        dataFromServerToClient = ("Another", "data", "batch", "to", "test", "server-client", "communication.")

        # act
        server.start()
        client.start()

        for i in range(0, len(dataFromClientToServer)):
            client.send(dataFromClientToServer[i])
            server.send(dataFromServerToClient[i])
            sleep(0.1)

        client.stop()
        server.stop()

        dataReceivedByClient = client.receivedHandler.receivedDataTuple
        dataReceivedByServer = server.receivedHandler.receivedDataTuple

        # assert
        self.assertEqual(dataReceivedByClient, dataFromServerToClient)
        self.assertEqual(dataReceivedByServer, dataFromClientToServer)
