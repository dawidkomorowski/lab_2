from unittest import TestCase

from Network.Core.Configuration import Configuration
from Network.Core.Server import Server


class TestServer(TestCase):
    def test_ShouldStartAndStopKindly_WhenNoClientConnected(self):
        # arrange
        configuration = Configuration()
        configuration.timeout = 0.01;
        server = Server(configuration)

        # act
        # assert
        server.start()
        server.stop()

    def test_ShouldPutDataInQueue_WhenSendCalled(self):
        # arrange
        server = Server()

        # act
        server.send("data")

        # assert
        self.assertEqual(server._sendQueue.get(), "data")
