from Xml.XmlSerializer import XmlValue


class ResponseStatus:
    Ok = 0
    Error = 1


class Response:
    data = XmlValue(str)
    status = XmlValue(int)

    @staticmethod
    def create(data="", status=ResponseStatus.Ok):
        response = Response()
        response.data = data
        response.status = status
        return response
