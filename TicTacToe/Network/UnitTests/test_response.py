from unittest import TestCase

from Network.Response import Response, ResponseStatus
from Xml.XmlSerializer import XmlSerializer


class TestResponse(TestCase):
    def test_ShouldCreateEmptyResponse(self):
        # arrange
        # act
        response = Response.create()

        # assert
        self.assertIsInstance(response, Response)
        self.assertEqual(response.data, "")
        self.assertEqual(response.status, ResponseStatus.Ok)

    def test_ShouldCreateResponse_GivenParameters(self):
        # arrange
        data = "Test data"
        status = ResponseStatus.Error

        # act
        response = Response.create(data, status)

        # assert
        self.assertIsInstance(response, Response)
        self.assertEqual(response.data, data)
        self.assertEqual(response.status, status)

    def test_ShouldSerializeAndDeserialize(self):
        # arrange
        data = "Test data"
        status = ResponseStatus.Error

        response = Response.create(data, status)

        # act
        serializedResponse = XmlSerializer.serialize(response, Response)
        deserializedResponse = XmlSerializer.deserialize(serializedResponse, Response)

        # assert
        self.assertIsInstance(deserializedResponse, Response)
        self.assertEqual(deserializedResponse.data, data)
        self.assertEqual(deserializedResponse.status, status)
