from _socket import error
from unittest import TestCase

from Network.Core.Configuration import Configuration
from Network.RRClient import RRClient


class TestRRClient(TestCase):
    def test_ShouldRaiseTimeout_WhenNoServer(self):
        # arrange
        configuration = Configuration()
        configuration.timeout = 0.01
        client = RRClient(configuration)

        # act
        # assert
        with self.assertRaises(error):
            client.start()
