from unittest import TestCase

from Network.Core.Configuration import Configuration
from Network.RRServer import RRServer


class TestRRServer(TestCase):
    def test_ShouldStartAndStopKindly_WhenNoClientConnected(self):
        # arrange
        configuration = Configuration()
        configuration.timeout = 0.01;
        server = RRServer(configuration)

        # act
        # assert
        server.start()
        server.stop()
