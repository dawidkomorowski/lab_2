import thread
from unittest import TestCase

from Network.Core.Configuration import Configuration
from Network.RRClient import RRClient
from Network.RRServer import RRServer, TimeoutExceededException
from Network.Request import Request
from Network.Response import Response, ResponseStatus
from Network.UnitTests.TestData import TestRRClientService


class TestRRClientRRServer(TestCase):
    def test_ShouldClientConnectAndDisconnectKindly(self):
        # arrange
        client = RRClient()
        server = RRServer()

        # act
        # assert
        server.start()
        client.start()
        client.stop()
        server.stop()

    def test_ShouldClientReceiveButNotRespond_WhenServerSendRequest(self):
        # arrange
        configuration = Configuration()
        configuration.rrTimeout = 1;
        client = RRClient(configuration, TestRRClientService())
        server = RRServer(configuration)

        request = Request.create("Test data from server to client", "text/plain")
        exception = None

        # act
        server.start()
        client.start()

        thread.start_new(client.run, ())

        try:
            server.sendRequest(request)
        except TimeoutExceededException as e:
            exception = e

        client.stop()
        server.stop()

        clientReceivedRequest = client._service.lastReceivedRequest

        # assert
        self.assertIsInstance(exception, TimeoutExceededException)
        self.assertIsInstance(clientReceivedRequest, Request)
        self.assertEqual(clientReceivedRequest.data, request.data)
        self.assertEqual(clientReceivedRequest.header, request.header)

    def test_ShouldClientSendResponse_WhenServerSendRequest(self):
        # arrange
        configuration = Configuration()
        client = RRClient(configuration, TestRRClientService())
        server = RRServer(configuration)

        request = Request.create("Test data from server to client", "text/plain")

        # act
        server.start()
        client.start()

        thread.start_new(client.run, ())

        response = server.sendRequest(request)

        client.stop()
        server.stop()

        # assert
        self.assertIsInstance(response, Response)
        self.assertEqual(response.status, ResponseStatus.Ok)
        self.assertEqual(response.data, request.header + "\n" + request.data)

    def test_ClientServerCommunicationScenario(self):
        # arrange
        configuration = Configuration()
        client = RRClient(configuration, TestRRClientService())
        server = RRServer(configuration)

        requests = (Request.create("First request.", "text/plain"), Request.create("Second request.", "text/plain"),
                    Request.create("Third request.", "text/plain"), Request.create("And last request.", "command"))

        responses = ()

        # act
        server.start()
        client.start()

        thread.start_new(client.run, ())

        for r in requests:
            responses += (server.sendRequest(r),)

        client.stop()
        server.stop()

        # assert
        self.assertEqual(len(responses), 4)
        for r in responses:
            self.assertIsInstance(r, Response)
            self.assertEqual(r.status, ResponseStatus.Ok)
            self.assertEqual(r.data, requests[responses.index(r)].header + "\n" + requests[responses.index(r)].data)
