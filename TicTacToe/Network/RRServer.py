import logging
from Queue import Queue, Empty

from Network.Core.Configuration import Configuration
from Network.Core.Server import Server
from Network.Request import Request
from Network.Response import Response
from Network.ResponseReceivedHandler import ResponseReceivedHandler
from Xml.XmlSerializer import XmlSerializer


class RRServer:
    logger = logging.getLogger(__name__)

    def __init__(self, configuration=Configuration()):
        self.logger.debug("Initializing request-response server.")

        self._configuration = configuration
        self._server = Server(configuration)
        self._server.receivedHandler = ResponseReceivedHandler(self)

        self._receivedData = Queue(1)

    def start(self):
        self.logger.info("Starting request-response server.")
        self._server.start()

    def stop(self):
        self.logger.info("Stopping request-response server.")
        self._server.stop()

    def sendRequest(self, request):
        self.logger.debug("Sending request to client.")

        requestData = XmlSerializer.serialize(request, Request)
        self._server.send(requestData)
        try:
            responseData = self._receivedData.get(timeout=self._configuration.rrTimeout)
        except Empty:
            raise TimeoutExceededException()

        self.logger.debug("Received response from client.")

        response = XmlSerializer.deserialize(responseData, Response)
        return response


class TimeoutExceededException(Exception):
    pass
