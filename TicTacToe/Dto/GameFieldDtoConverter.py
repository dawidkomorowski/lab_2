from Dto.AbstractDtoConverter import AbstractDtoConverter
from Dto.GameFieldDto import GameFieldDto
from Game.GameField import GameEmptyField, GameXField, GameOField


class GameFieldDtoConverter(AbstractDtoConverter):
    EmptyField = "EMPTY_FIELD"
    XField = "X_FIELD"
    OField = "O_FIELD"

    def fromDto(self, dto):
        if dto.field == GameFieldDtoConverter.EmptyField:
            return GameEmptyField
        if dto.field == GameFieldDtoConverter.XField:
            return GameXField
        if dto.field == GameFieldDtoConverter.OField:
            return GameOField

    def toDto(self, object):
        dto = GameFieldDto()

        if object is GameEmptyField:
            dto.field = GameFieldDtoConverter.EmptyField
            return dto
        if object is GameXField:
            dto.field = GameFieldDtoConverter.XField
            return dto
        if object is GameOField:
            dto.field = GameFieldDtoConverter.OField
            return dto
