from Dto.GamePlayerDto import GamePlayerDto
from Xml.XmlSerializer import XmlCollection, XmlValue


class GameMoveDto:
    gamePlayer = XmlCollection(GamePlayerDto, True)
    targetX = XmlValue(int)
    targetY = XmlValue(int)
