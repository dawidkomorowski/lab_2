from Dto.AbstractDtoConverter import AbstractDtoConverter
from Dto.GameStateDto import GameStateDto
from Game.GameState import GameState


class GameStateDtoConverter(AbstractDtoConverter):
    def __init__(self, gamePlayerDtoConverter, gameFieldDtoConverter):
        self._gamePlayerDtoConverter = gamePlayerDtoConverter
        self._gameFieldDtoConverter = gameFieldDtoConverter

    def fromDto(self, dto):
        gameState = GameState()
        gameState.currentPlayer = self._gamePlayerDtoConverter.fromDto(dto.currentPlayer)
        for x in range(0, 3):
            for y in range(0, 3):
                i = x + y * 3
                field = self._gameFieldDtoConverter.fromDto(dto.map[i])
                gameState.setFieldAt(x, y, field)

        return gameState

    def toDto(self, object):
        dto = GameStateDto()
        dto.currentPlayer = self._gamePlayerDtoConverter.toDto(object.currentPlayer)
        for i in range(0, 9):
            x = i % 3
            y = i / 3
            field = object.getFieldAt(x, y)
            dto.map += (self._gameFieldDtoConverter.toDto(field),)

        return dto
