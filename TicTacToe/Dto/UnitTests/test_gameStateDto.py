from unittest import TestCase

from Dto.GameFieldDto import GameFieldDto
from Dto.GameFieldDtoConverter import GameFieldDtoConverter
from Dto.GamePlayerDto import GamePlayerDto
from Dto.GamePlayerDtoConverter import GamePlayerDtoConverter
from Dto.GameStateDto import GameStateDto
from Game.GameField import GameEmptyField, GameOField
from Game.GameField import GameXField
from Game.GamePlayer import GameOPlayer
from Xml.XmlSerializer import XmlSerializer


class TestGameStateDto(TestCase):
    def test_ShouldSerializeAndDeserialize(self):
        # arrange
        gamePlayerDtoConverter = GamePlayerDtoConverter()
        gameFieldDtoConverter = GameFieldDtoConverter()

        dto = GameStateDto()
        dto.currentPlayer = gamePlayerDtoConverter.toDto(GameOPlayer)
        dto.map += (gameFieldDtoConverter.toDto(GameXField),)
        dto.map += (gameFieldDtoConverter.toDto(GameEmptyField),)
        dto.map += (gameFieldDtoConverter.toDto(GameOField),)
        dto.map += (gameFieldDtoConverter.toDto(GameOField),)
        dto.map += (gameFieldDtoConverter.toDto(GameXField),)
        dto.map += (gameFieldDtoConverter.toDto(GameEmptyField),)
        dto.map += (gameFieldDtoConverter.toDto(GameEmptyField),)
        dto.map += (gameFieldDtoConverter.toDto(GameOField),)
        dto.map += (gameFieldDtoConverter.toDto(GameXField),)

        # act
        serializedDto = XmlSerializer.serialize(dto, GameStateDto)
        deserializedDto = XmlSerializer.deserialize(serializedDto, GameStateDto)

        # assert
        self.assertIsInstance(deserializedDto, GameStateDto)
        self.assertIsInstance(deserializedDto.currentPlayer, GamePlayerDto)
        self.assertEqual(deserializedDto.currentPlayer.player, dto.currentPlayer.player)
        for i in range(0, 9):
            self.assertIsInstance(deserializedDto.map[i], GameFieldDto)
            self.assertEqual(deserializedDto.map[i].field, dto.map[i].field)
