from Dto.GameFieldDto import GameFieldDto
from Dto.GamePlayerDto import GamePlayerDto
from Xml.XmlSerializer import XmlCollection


class GameStateDto:
    currentPlayer = XmlCollection(GamePlayerDto, True)
    map = XmlCollection(GameFieldDto)

    def __init__(self):
        self.map = ()
