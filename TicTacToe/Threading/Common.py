from datetime import datetime, timedelta


class Common:
    @staticmethod
    def waitUntilTrue(callable, timeout=None):
        startTime = datetime.now()
        while callable() is not True:
            currentTime = datetime.now()
            b = callable()

            if timeout is None:
                continue
            if (currentTime - startTime) > timedelta(seconds=timeout):
                return
