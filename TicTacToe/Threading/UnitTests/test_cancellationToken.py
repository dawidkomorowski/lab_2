from unittest import TestCase

from Threading.CancellationToken import CancellationToken


class TestCancellationToken(TestCase):
    def test_ShouldNotBeCanceled_GivenFreshInstance(self):
        # arrange
        # act
        cancellationToken = CancellationToken()

        # assert
        self.assertFalse(cancellationToken.isCanceled())

    def test_ShouldBeCanceled_WhenCancelIsCalled(self):
        # arrange
        cancellationToken = CancellationToken()

        # act
        cancellationToken.cancel()

        # assert
        self.assertTrue(cancellationToken.isCanceled())
