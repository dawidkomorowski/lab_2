from Threading.UnitOfWork import UnitOfWork


class TestUnitOfWork(UnitOfWork):
    def __init__(self, stopThread=False):
        self.executed = False
        self.stopThread = stopThread

    def execute(self):
        self.executed = True
        if self.stopThread:
            self._cancellationToken.cancel()
