from abc import abstractmethod


class CliInputComponent:
    @abstractmethod
    def read(self, reader):
        pass
