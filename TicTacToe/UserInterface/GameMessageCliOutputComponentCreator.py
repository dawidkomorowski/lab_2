from Game.GameRules.AfterMovePerspective.DrawGameRule import DrawGameRuleException
from Game.GameRules.AfterMovePerspective.PlayerWonGameRule import PlayerWonGameRuleException
from Game.GameRules.BeforeMovePerspective.ValidateGameNotFinishedGameRule import GameAlreadyFinishedGameRuleException
from Game.GameRules.BeforeMovePerspective.ValidateMoveTargetPositionGameRule import \
    InvalidTargetPositionGameRuleException
from Game.GameRules.BeforeMovePerspective.ValidatePlayerGameRule import InvalidPlayerGameRuleException
from UserInterface.Common import Common
from UserInterface.GameMessageCliOutputComponent import GameMessageCliOutputComponent


class GameMessageCliOutputComponentCreator:
    MessageNone = ""
    MessageInvalidPlayer = "Invalid player tried to make move. It is turn of opposite player."
    MessageInvalidTargetPosition = "Selected position for move is incorrect. Choose different position."
    MessageGameAlreadyFinished = "Game is already finished. Restart game to play again."
    MessagePlayerWon = "Game is over. The winner is "
    MessageDraw = "Game is over. No player has won. The result is draw."

    def createGameMessage(self, gameRuleException):
        if gameRuleException is None:
            return GameMessageCliOutputComponent(GameMessageCliOutputComponentCreator.MessageNone)
        if isinstance(gameRuleException, InvalidPlayerGameRuleException):
            return GameMessageCliOutputComponent(GameMessageCliOutputComponentCreator.MessageInvalidPlayer)
        if isinstance(gameRuleException, InvalidTargetPositionGameRuleException):
            return GameMessageCliOutputComponent(GameMessageCliOutputComponentCreator.MessageInvalidTargetPosition)
        if isinstance(gameRuleException, GameAlreadyFinishedGameRuleException):
            return GameMessageCliOutputComponent(GameMessageCliOutputComponentCreator.MessageGameAlreadyFinished)
        if isinstance(gameRuleException, PlayerWonGameRuleException):
            return GameMessageCliOutputComponent(
                    GameMessageCliOutputComponentCreator.MessagePlayerWon + Common.playerToString(
                            gameRuleException.winner) + ".")
        if isinstance(gameRuleException, DrawGameRuleException):
            return GameMessageCliOutputComponent(GameMessageCliOutputComponentCreator.MessageDraw)
