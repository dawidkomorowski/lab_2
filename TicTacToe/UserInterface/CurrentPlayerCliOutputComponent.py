from UserInterface.CliOutputComponent import CliOutputComponent
from UserInterface.Common import Common


class CurrentPlayerCliOutputComponent(CliOutputComponent):
    def __init__(self, gameState):
        self._gameState = gameState

    def write(self, writer):
        writer.write("Turn of " + Common.playerToString(self._gameState.currentPlayer) + ":")
