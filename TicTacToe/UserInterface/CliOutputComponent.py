from abc import abstractmethod


class CliOutputComponent:
    @abstractmethod
    def write(self, writer):
        pass
