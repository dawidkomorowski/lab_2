from UserInterface.InputOutput.Readers.AbstractReader import AbstractReader


class ConsoleReader(AbstractReader):
    def read(self):
        return raw_input()
