from StringIO import StringIO

from UserInterface.InputOutput.Readers.AbstractReader import AbstractReader


class StringReader(AbstractReader):
    def __init__(self, input):
        self._input = StringIO(input)

    def read(self):
        return self._input.readline()
