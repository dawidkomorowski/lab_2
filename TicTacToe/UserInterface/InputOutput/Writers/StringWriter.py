from UserInterface.InputOutput.Writers.AbstractWriter import AbstractWriter


class StringWriter(AbstractWriter):
    def __init__(self):
        self.output = ""

    def write(self, message):
        self.output += message + "\n"
