from Game.GamePlayer import GameXPlayer, GameOPlayer, UnknownGamePlayerTypeException


class Common:
    StringXPlayer = "X-player"
    StringOPlayer = "O-player"

    @staticmethod
    def playerToString(gamePlayer):
        if gamePlayer is GameXPlayer:
            return Common.StringXPlayer
        if gamePlayer is GameOPlayer:
            return Common.StringOPlayer

        raise UnknownGamePlayerTypeException(gamePlayer)
