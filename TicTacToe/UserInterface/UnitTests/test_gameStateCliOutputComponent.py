from unittest import TestCase

from Game.GameField import GameOField, GameEmptyField, GameXField
from Game.GameState import GameState
from UserInterface.GameStateCliOutputComponent import GameStateCliOutputComponent
from UserInterface.InputOutput.Writers.StringWriter import StringWriter


class TestGameStateCliOutputComponent(TestCase):
    def test_ShouldWriteCorrectEmptyGameState(self):
        # arrange
        stringWriter = StringWriter()
        gameState = GameState()
        gameStateCliComponent = GameStateCliOutputComponent(gameState)

        expectedOutput = """
+---+---+---+
| 1 | 2 | 3 |
+---+---+---+
| 4 | 5 | 6 |
+---+---+---+
| 7 | 8 | 9 |
+---+---+---+
"""

        # act
        stringWriter.write("")
        gameStateCliComponent.write(stringWriter)

        # assert
        self.assertEqual(stringWriter.output, expectedOutput)

    def test_ShouldWriteCorrectNotEmptyGameState(self):
        # arrange
        stringWriter = StringWriter()
        gameState = GameState()
        gameState.map = [[GameXField, GameOField, GameOField],
                         [GameEmptyField, GameXField, GameEmptyField],
                         [GameXField, GameEmptyField, GameEmptyField]]
        gameStateCliComponent = GameStateCliOutputComponent(gameState)

        expectedOutput = """
+---+---+---+
| x | o | o |
+---+---+---+
| 4 | x | 6 |
+---+---+---+
| x | 8 | 9 |
+---+---+---+
"""

        # act
        stringWriter.write("")
        gameStateCliComponent.write(stringWriter)

        # assert
        self.assertEqual(stringWriter.output, expectedOutput)
